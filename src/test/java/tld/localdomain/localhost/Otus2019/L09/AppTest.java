//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-08 13:15:55 korskov>

package tld.localdomain.localhost.Otus2019.L09;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
// import org.junit.runner.RunWith;
import org.junit.runners.Suite;

// @RunWith(Suite.class)

// TestSuite_Executor.class
@Suite.SuiteClasses({
        TestSuite_Smoke.class,
        TestSuite_Account.class
})

public class AppTest {}
