//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-19 15:23:09 korskov>

package tld.localdomain.localhost.Otus2019.L09;

/**
 * L9 Homemade ORM
 *
 * Создайте класс User (с полями, которые соответствуют таблице, поле
 * id отметьте аннотацией).
 *
 * Создайте в базе таблицу User с полями:
 * 1. id bigint(20) NOT NULL auto_increment
 * 2. name varchar(255)
 * 3. age int(3)
 */

import java.util.Objects;

class User {
    @Id
    private long id;
    private String name;
    private byte age;

    User(){;}

    User(long id, String name, byte age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    User(long id, String name, int age) {
        this(id, name, (byte)age);
    }

    public void setAge(int n) {
        age = (byte)n;
    }

    @Override
    public boolean equals(final Object obj) {
        if(obj == this)
            return true;
        if(obj == null || getClass() != obj.getClass())
            return false;
        User uobj = (User) obj;
        return uobj.id == id //
            && uobj.age == age //
            && Objects.equals(name, uobj.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age);
    }

    @Override
    public String toString() {
        return String.format("id=%d  name='%s'  age=%d", id, name, age);
    }
}
