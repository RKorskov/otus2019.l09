//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-19 15:22:59 korskov>

package tld.localdomain.localhost.Otus2019.L09;

/**
 * L9 Homemade ORM
 *
 * Создайте еще одну таблицу Account:
 * 1. no bigint(20) NOT NULL auto_increment
 * 2. type varchar(255)
 * 3. rest number
 *
 * Создайте для этой таблицы класс Account. Проверьте работу
 * JdbcTemplate на этом классе.
 */

import java.util.Objects;

class Account {
    @Id
    private long no;
    private String type;
    // https://sourceforge.net/projects/jmfp/
    // ... https://github.com/tools4j/decimal4j
    // Decimal rest;
    //float rest; // S.E.P.
    private long rest; // fixed point #*#.##

    Account() {;}

    Account(long no, String type, int rest) {
        this.no = no;
        this.type = type;
        this.rest = rest;
    }

    Account(long no, String type, long rest) {
        this.no = no;
        this.type = type;
        this.rest = rest;
    }

    Account(long no, String type, double rest) {
        this.no = no;
        this.type = type;
        this.rest = (long)(rest * 100);
    }

    public long add(final long rdiff) {
        rest += rdiff;
        return rest;
    }

    @Override
    public boolean equals(final Object obj) {
        if(obj == this)
            return true;
        if(obj == null || getClass() != obj.getClass())
            return false;
        Account aobj = (Account) obj;
        return aobj.no == no //
            && aobj.rest == rest //
            && Objects.equals(type, aobj.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(no, type);
    }

    @Override
    public String toString() {
        return String.format("id=%d  type='%s'  rest=%d", no, type, rest);
    }
}
