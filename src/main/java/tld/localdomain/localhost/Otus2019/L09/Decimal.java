//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-07 17:33:11 korskov>

package tld.localdomain.localhost.Otus2019.L09;

import java.util.Objects;
import java.lang.Math;

/**
 * Fixed-point numbers
 *
 * ival.frac
 */

final class Decimal {
    private final static short LENGTH_MAX = 19; // floor(log10(2^64-1)) - 1
    private final static long FRAC_MUL = 100_0000_0000_0000_0000L; // 1e19
    private final static long[] FRAC_MULS = {
        1,
        10,
        100,
        1000,
        1_0000,
        10_0000,
        100_0000,
        1000_0000,
        1_0000_0000,
        10_0000_0000L,
        100_0000_0000L,
        1000_0000_0000L,
        1_0000_0000_0000L,
        10_0000_0000_0000L,
        100_0000_0000_0000L,
        1000_0000_0000_0000L,
        1_0000_0000_0000_0000L,
        10_0000_0000_0000_0000L,
        100_0000_0000_0000_0000L
    };
    final long ival, frac;
    final short length_ival, length_frac;

    Decimal(long n) {
        ival = n;
        frac = 0;
        length_ival = 19;
        length_frac = 0;
    }

    Decimal(double n) {
        ival = (long)n;
        frac = (long)(Math.abs(n) - Math.abs(ival)) * FRAC_MUL;
        length_ival = LENGTH_MAX;
        length_frac = LENGTH_MAX;
    }

    Decimal(double n, int flen) {
        ival = (long)n;
        frac = (long)(Math.abs(n) - Math.abs(ival)) * FRAC_MULS[flen];
        length_ival = LENGTH_MAX;
        length_frac = (short)flen;
    }

    Decimal(double n, int ilen, int flen) {
        ival = (long)n; // fixme!
        frac = (long)(Math.abs(n) - Math.abs(ival)) * FRAC_MULS[flen];
        length_ival = (short)ilen;
        length_frac = (short)flen;
    }
}
