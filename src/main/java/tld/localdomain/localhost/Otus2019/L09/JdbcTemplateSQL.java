//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-19 16:53:01 korskov>

package tld.localdomain.localhost.Otus2019.L09;

/**
 * L9 Homemade ORM
 *
 * релизация JdbcTemplate для (my)sql
 *
 * Напишите JdbcTemplate, который умеет работать с классами, в которых
 * есть поле с аннотацией @Id. JdbcTemplate должен сохранять объект в
 * базу и читать объект из базы. Имя таблицы должно соответствовать
 * имени класса, а поля класса - это колонки в таблице. Проверьте его
 * работу на классе User.
 *
 * Методы JdbcTemplate'а:  
 * ```java
 * void create(T objectData);
 * void update(T objectData);
 * void createOrUpdate(T objectData);
 * <T> T load(long id, Class<T> clazz);
 * ```
 */

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.IllegalAccessException;
import java.lang.NoSuchMethodException;
import java.lang.InstantiationException;
import java.lang.reflect.InvocationTargetException;

class JdbcTemplateSQL implements JdbcTemplate {
    private static final String SQL_DATABASE = "L09ORMDB",
        SELECT_BY = "select * from `" + SQL_DATABASE + "`.`%s` where `%s` = ?";

    public <T> void create(final T objectData) {
        if(objectData == null)
            return;
        final Class clazz = objectData.getClass();
        try {
            final Field fid = getFieldId(clazz);
            fid.setAccessible(true);
            try (Connection dbc = DriverManager.getConnection //
                 (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
                String query = buildSQLInsert(clazz);
                try (PreparedStatement pds = dbc.prepareStatement(query)) {
                    final long id = fid.getLong(objectData); // fixme! primary key (@Id) may be not number at all
                    int n = 0;
                    for(Field f : clazz.getDeclaredFields()) {
                        ++n;
                        f.setAccessible(true);
                        pds.setString(n, f.get(objectData).toString());
                    }
                    pds.execute();
                }
                catch (SQLException|IllegalAccessException ex) {;}
            }
            catch (SQLException ex) {;}
        }
        catch (NoSuchFieldException ex) {;}
    }

    public <T> void update(final T objectData) {
        if(objectData == null)
            return;
        final Class clazz = objectData.getClass();
        try {
            final Field fid = getFieldId(clazz);
            fid.setAccessible(true);
            try (Connection dbc = DriverManager.getConnection //
                 (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
                final long id = fid.getLong(objectData); // fixme! primary key (@Id) may be not number at all
                String query = buildSQLUpdate(clazz);
                try (PreparedStatement pds = dbc.prepareStatement(query)) {
                    int n = 0;
                    for(Field f : clazz.getDeclaredFields()) {
                        f.setAccessible(true);
                        pds.setString(++n, f.get(objectData).toString());
                    }
                    pds.execute();
                }
                catch (SQLException|IllegalAccessException ex) {;}
            }
            catch (SQLException|IllegalAccessException ex) {;}
        }
        catch (NoSuchFieldException ex) {;}
    }

    public <T> void createOrUpdate(final T objectData) {
        if(objectData == null)
            return;
        final Class clazz = objectData.getClass();
        try {
            final Field fid = getFieldId(clazz);
            fid.setAccessible(true);
            try (Connection dbc = DriverManager.getConnection //
                 (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
                final long id = fid.getLong(objectData); // fixme! primary key (@Id) may be not number at all
                String query = isPresent(dbc, objectData, fid, id) //
                    ? buildSQLUpdate(clazz) //
                    : buildSQLInsert(clazz);
                try (PreparedStatement pds = dbc.prepareStatement(query)) {
                    int n = 0;
                    for(Field f : clazz.getDeclaredFields()) {
                        f.setAccessible(true);
                        pds.setString(++n, f.get(objectData).toString());
                    }
                    pds.execute();
                }
                catch (SQLException|IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
            catch (SQLException|IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }
        catch (NoSuchFieldException ex) {;}
    }

    public <T> T load(final long id, final Class<T> clazz) {
        T obj = null;
        try {
            final Field fid = getFieldId(clazz);
            fid.setAccessible(true);
            try (Connection dbc = DriverManager.getConnection //
                 (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
                String query = String.format //
                    (SELECT_BY, clazz.getSimpleName(), fid.getName());
                try (PreparedStatement pds = dbc.prepareStatement(query)) {
                    pds.setLong(1, id);
                    try (ResultSet rs = pds.executeQuery()) {
                        rs.first();
                        obj = clazz.getDeclaredConstructor().newInstance();
                        for(Field f : clazz.getDeclaredFields()) {
                            f.setAccessible(true);
                            f.set(obj, //
                                  rs.getObject(f.getName(), //
                                               f.getType()));
                        }
                    }
                    catch(InstantiationException //
                          | NoSuchMethodException //
                          | InvocationTargetException ex) {
                        return null;
                    }
                }
            }
            catch (SQLException|IllegalAccessException ex) {
                return null;
            }
        }
        catch (NoSuchFieldException ex) {
            return null;
        }
        return obj;
    }

    private static void evalSQL(final Connection dbc, final String query) //
        throws SQLException {
        try (PreparedStatement pds = //
             dbc.prepareStatement(query)) {
            pds.execute();
        }
    }

    private static <T> Field getFieldId(final Class<T> clazz) //
        throws NoSuchFieldException {
        for(Field f : clazz.getDeclaredFields()) {
            for(Annotation a : f.getDeclaredAnnotations()) {
                if(a instanceof Id)
                    return f;
            }
        }
        throw new NoSuchFieldException();
    }

    private static <T> String buildSQLInsert(final Class<T> clz) {
        StringBuilder qhead = new StringBuilder("insert into `"),
            qtail = new StringBuilder(") values(");
        qhead.append(SQL_DATABASE);
        qhead.append("`.`");
        qhead.append(clz.getSimpleName()); // qhead.append("` (");
        boolean isn1st = false;
        for(Field f : clz.getDeclaredFields()) {
            f.setAccessible(true);
            if(isn1st) {
                qhead.append(",`");
                qtail.append(",?");
            }
            else {
                qhead.append("` (`");
                qtail.append('?');
                isn1st = true;
            }
            qhead.append(f.getName());
            qhead.append('`');
        }
        //qhead.append(") ");
        qtail.append(')');
        qhead.append(qtail);
        return qhead.toString();
    }

    private static <T> String buildSQLUpdate(final Class<T> clz) {
        // update DB.table set c0=v0, c1=v1, ..., cn=vn;
        StringBuilder qhead = new StringBuilder("update `");
        qhead.append(SQL_DATABASE);
        qhead.append("`.`");
        qhead.append(clz.getSimpleName()); // qhead.append('`');
        boolean isn1st = false;
        for(Field f : clz.getDeclaredFields()) {
            f.setAccessible(true);
            if(isn1st) {
                qhead.append(", `");
            }
            else {
                qhead.append("` set `");
                isn1st = true;
            }
            //qhead.append('`');
            qhead.append(f.getName());
            qhead.append("` = ?");
        }
        return qhead.toString();
    }

    private static <T> boolean isPresent(final Connection dbc,
                                         final T objectData,
                                         final Field fid, final long id) //
        throws SQLException {
        fid.setAccessible(true);
        String query = String.format (SELECT_BY,
                                      objectData.getClass().getSimpleName(),
                                      fid.getName());
        try (PreparedStatement pds = dbc.prepareStatement(query)) {
            pds.setLong(1, id);
            try (ResultSet rs = pds.executeQuery()) {
                while(rs.next()) {
                    long uid = rs.getLong(1);
                    if(id == uid)
                        return true;
                }
            }
        }
        return false;
    }
}
