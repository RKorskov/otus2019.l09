-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-08-12 12:24:09 roukoru>

Overview
========

otus-java_2019-03

Группа 2019-03  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov/otus-java_2018-12)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L9 Homemade ORM #

Работа должна использовать базу данных H2.  
Создайте в базе таблицу User с полями:
1. id bigint(20) NOT NULL auto_increment
2. name varchar(255)
3. age int(3)

Создайте свою аннотацию @Id .

Создайте класс User (с полями, которые соответствуют таблице, поле id отметьте аннотацией).

Напишите JdbcTemplate, который умеет работать с классами, в которых
есть поле с аннотацией @Id. JdbcTemplate должен сохранять объект в
базу и читать объект из базы. Имя таблицы должно соответствовать имени
класса, а поля класса - это колонки в таблице. Проверьте его работу на
классе User.

Методы JdbcTemplate'а:  
```java
void create(T objectData);
void update(T objectData);
void createOrUpdate(T objectData); // опционально.
<T> T load(long id, Class<T> clazz);
```

Метод createOrUpdate - необязательный.
Он должен "проверять" наличие объекта в таблице и создавать новый или обновлять.

Создайте еще одну таблицу Account:
1. no bigint(20) NOT NULL auto_increment
2. type varchar(255)
3. rest number

Создайте для этой таблицы класс Account и проверьте работу JdbcTemplate на этом классе.

Еще одна опция (по желанию):
Реализовать абстракцию DBService для объектов User и Account.

И еще одна опция (по желанию для супер-мега крутых бизонов):
прикрутите этот "jdbc-фреймворк" к департаменту ATM.

TODO
====

[说谈的老师](https://otus.ru/learning/11421/#/homework-chat/2748/)

В этой группе ДЗ через Pull Request-ы сдаются, так намного удобнее проверять и комментировать.

1) не забывайте классы по пакетам раскладывать.

2) class User {
    @Id
    long id;
    String name;
    byte age;}
    
поля должны быть private

3) public boolean equals - есть, а hashCode нет - они всегда ходят парой

4) class Account аналогичные замечания.

5) DBService, Decimal - зачем нужны эти классы?

6) public interface SettingsMySQL - интерфейс - не самое удачное место для объявления констант.

7) class JdbcTemplateSQL for(;rs.next();) {
                    long uid = rs.getLong(1);
                    if(id == uid)
                        return true;
                } тут больше while подходит 8) private static <T> Field getFieldId(final Class<T> clazz) {
        for(Field f : clazz.getDeclaredFields()) {
            for(Annotation a : f.getDeclaredAnnotations()) {
                if(a instanceof Id)
                    return f;
            }
        }
        return null;
    }

тут вместо null лучше сразу Exception бросить, чем потом с обработкой null заморачиваться.

9) комментарии `fixme! primary key (@Id) may be not number at all` разумны, поле c @id действительно, обязательно должно быть.

Поправьте, пожалуйста, эти замечания. С ними работа смотрится весьма неряшливо.
